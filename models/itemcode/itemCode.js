const itemCode =
  "SELECT segment1||'-'||segment3||'-'||segment5 item_code, inventory_item_id FROM mtl_system_items msi WHERE organization_id = 85 AND INVENTORY_ITEM_STATUS_CODE = 'Active' AND ATTRIBUTE20 IS NOT NULL ORDER BY 1";


const test = ((p_itemCode, p_itemID) => {
const queries = `SELECT segment1 || '-' || segment3 || '-' || segment5 item_code, inventory_item_id, ATTRIBUTE20 category, ATTRIBUTE18 merk, ATTRIBUTE17 manufacture FROM mtl_system_items msi WHERE organization_id = 85 AND INVENTORY_ITEM_STATUS_CODE = 'Active' AND ATTRIBUTE20 IS NOT NULL AND segment1 || '-' || segment3 || '-' || segment5 = ${p_itemCode} AND inventory_item_id = ${p_itemID} ORDER BY 1` 
return queries
})

export default {
  itemCode,
  test,
};
