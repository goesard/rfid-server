import express from "express";
import oracledb from "oracledb";
import queries from "../../models/itemcode/itemCode.js";

const app = express();

const itemCodeLOV = app.get("/itemCode", async (req, res) => {
  try {
    const connection = await oracledb.getConnection("APPS");
    const result = await connection.execute(queries.itemCode, [], {
      outFormat: oracledb.OUT_FORMAT_OBJECT,
    });
    return res.status(200).json(result.rows);
  } catch (e) {
    res.status(500).json(e);
    console.error(e);
  }
});

const autofillLOV = app.get("/autofill", async (req, res) => {
  try {
    const p_itemCode = req.query.p_item;
    const p_itemID = req.query.p_itemID;
    const connection = await oracledb.getConnection("APPS");
    const result = await connection.execute(
      queries.test(p_itemCode, p_itemID),
      [],
      {
        outFormat: oracledb.OUT_FORMAT_OBJECT,
      }
    );

    await connection.close();
    console.log(`Item Code ${p_itemCode}`);
    console.log(`item ID ${p_itemID}`);
    return res.status(200).json(result.rows);
  } catch (e) {
    res.status(500).json(e);
    console.log(e);
  }
});

export default {
  itemCodeLOV,
  autofillLOV,
};
