import express from "express";
import dotenv from "dotenv";
import connection from "./connection/connection.js";
dotenv.config();

import router from "./routes/itemcode/itemCode.js"

const app = express();

app.get("/", (req, res) => {
  res.json("test");
});

app.use("/api/v1", router)

connection().then(() => {
  app.listen(process.env.PORT, () => {
    console.log(`Listing on http://localhost:${process.env.PORT}`);
  });
});
