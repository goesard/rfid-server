// const { Router } = require("express");
// const bodyParser = require("body-parser");
// const cors = require("cors");

import { Router } from "express";
import bodyParser from "body-parser";
import cors from "cors"
import controllers from "../../controllers/itemcode/itemCode.js"

// const controllers = require("../../controllers/itemcode/itemCode");

const router = Router();
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());
router.use(cors());

router.get("/itemCode", controllers.itemCodeLOV);
router.get("/autofill", controllers.autofillLOV);

export default router
