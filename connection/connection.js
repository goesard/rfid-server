import oracledb from "oracledb";
import dotenv from "dotenv";

// const oracledb = require("oracledb");
// const dotenv = require("dotenv");

dotenv.config();

const dbConfig = {
  user: "apps",
  password: "apps",
  connectString:
    "(DESCRIPTION =(ADDRESS = (PROTOCOL = TCP)(HOST = 172.16.42.131)(PORT = 1531))(CONNECT_DATA =(SID= BTSDEV)))",
  externalAuth: false, // Set to true if using External Authentication
  poolMin: 10,
  poolMax: 50,
  poolIncrement: 10,
  poolPingInterval: 60,
  poolTimeout: 1,
  queueTimeout: 0,
  poolAlias: 'APPS'
};

async function initOracleClient() {
  try {
    await oracledb.initOracleClient({ libDir: "D:/instantclient_21_12" });
    console.log("Oracle client initialized successfully");
  } catch (error) {
    console.error("Error initializing Oracle client:", error.message);
    process.exit(1);
  }
}

// Initialize Oracle client
async function initpool() {
  try {
    await oracledb.createPool(dbConfig);
    console.log("Oracle pool created successfully");
  } catch (error) {
    console.error("Error creating Oracle pool:", error.message);
    process.exit(1);
  }
}

async function connection() {
  try {
    initOracleClient();
    initpool();
    console.log("Connection Success");
  } catch (e) {
    console.error(`Error ${e}`);
  }
}

export default connection;
